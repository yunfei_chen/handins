#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.all.sh - displays entries in the current directory that match all of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ALL of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched all globs
		1 if no name matched all globs
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*'
		abc123

	EOF
	exit 2
} >&2
if [ "$*" = " " ]; then
	exit 2
fi
flagMatched=0
for arg in "$@"
do
	names=$(echo $(find . -name "$arg") | tr " " "\n" | sort -u)
	if [ "$names" = " " ]; then
		flagMatched=1
	fi
	if [ "$flagMatched" -eq 0 ]; then
		echo "$names"
	fi
done
if [ "$flagMatched" -eq 0 ]
then
	exit 0
else
	exit 1
fi
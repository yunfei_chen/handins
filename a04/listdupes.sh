#!/bin/sh

cat >/dev/null <<- EOF
	NAME
	listdupes.sh - Compares file(s) with other files to list duplicates

	SYNOPSIS
	listdupes.sh FILE...
	listdupes.sh FILE
	listdupes.sh

	DESCRIPTION
	When more than one argument is provided (1st form):
	Compares the first argument to all of the other arguments.

	When exactly one argument is provided (2nd form):
	Compares the file given as the argument to all of the other files in the current directory.

	When no arguments are provided (3rd form):
	Compares all files in the current directory to all other files in the current directory.

	EXIT STATUS
	0 if no duplicates were found
	1 if a duplicate was found
	2 if any of the given arguments are non-file items

	NOTES
	1) files that do not exist are silently ignored
	2) a file is never compared to itself (decided by having an identical name)
	3) a file is only displayed if it has duplicates
	4) duplicates are displayed as:
	    the given source file on a line
	    each of its duplicates, one per line
	    a blank line
	5) when no arguments are given, all combinations of duplicates will be displayed (see last example)
	6) IMPLEMENTATION
	a) check the arguments for non-files first. Remeber that non-existant files are ok, but arguments that are something other than a file are not okay. [~6 lines]
	b) create a function named: 'dupesOfFirst'
		* accepts a list of filenames as arguments
		* compares the first argument against all of the others
		* all comparison and display logic should be in here
		* [~20 lines]
	c) main body of script detects the number of arguments and calls 'dupesOfFirst' with the appropriate arguments [~10 lines]

	EXAMPLE

	\$ rm -rf tmp3; echo '1' > tmp1; echo '2' > tmp2; ./listdupes.sh tmp1 tmp2 tmp3 ; echo $?
	0


	\$ rm -rf tmp{1..3} ; ./listdupes.sh tmp1 tmp2 tmp3 ; echo $?
	0

	\$ mkdir tmpdir ; ./listdupes.sh tmpdir ; echo $? ; rm -rf tmpdir
	not a file: 'tmpdir'
	2

	\$ echo 0 > tmp01 ; cp tmp01 tmp02; cp tmp02 tmp03 ; echo 1 > tmp11; cp tmp11 tmp12; echo 2 > tmp21 ; ./listdupes.sh tmp01 ; echo $?
	tmp01
	tmp02
	tmp03

	1

	\$ echo 0 > tmp01 ; cp tmp01 tmp02; cp tmp02 tmp03 ; echo 1 > tmp11; cp tmp11 tmp12; echo 2 > tmp21 ; ./listdupes.sh ; echo $?
	tmp01
	tmp02
	tmp03

	tmp02
	tmp01
	tmp03

	tmp03
	tmp01
	tmp02

	tmp11
	tmp12

	tmp12
	tmp11

	1
EOF
dupesOfFirst(){
	matched=1
	printThis=1
	for arg in "$@"
	do
		counter=0
		while [ "$counter" -lt "$#" ]; do
			if [ $( cmp $args $counter 2>/dev/null)  -eq 0 ]; then
				printThis=0
				echo "$args \n $counter"
			fi
		done
	done
	
}
dupesOfFirst $@